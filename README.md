# Similar Product Recommendation

Manage product catalog and provide similar product recommendation by dominant color.

## Global schema
![architecture.png](architecture.png)

## Main modules
### CSV input function
As the trigger of the component's workflow, this module : 

  - listens to input files inside the provided GCS bucket
  - parses the content including at least unique product id and url of product's photo url
  - injects parsed 

### Vision refresher function
Periodically triggered by a scheduler, this module aims to :

  - downloads product images to a GCS bucket, because some producer's server could forbid GCP bots 
  
if they exceed a certain rate limit

  - calls the [Vision API](https://cloud.google.com/vision/) for every image and fetch dominant color information
  - persists useful information to BigQuery tables

### Matching function
This module is responsible to compute nearest neighbor by [color proximity](https://github.com/gausie/colour-proximity), 
and store refreshed candidates in a final BigQuery table.

### Client API for recommended product
As the output of the component, this module exposes a Web API allowing users to get parameterized N recommended 
similar products by given a product id.

A local cache is maintained by a scheduled task for every 30 minutes.

## Requirements

  - [Node](https://nodejs.org) 10 or above
  - [Google Cloud Platform](https://cloud.google.com/) account with required accessibility
  - [Terraform](https://www.terraform.io/)

## Deployment
Use [Google Cloud CLI](https://cloud.google.com/sdk/) to switch your GCP project and login.

You may see errors by disabled APIs, just follow the link of terraform CLI to activate them.

You can deploy by this repo's pipeline.

Or deploy cloud functions manually:
```
(Windows)
application\<function_folder> .\deploy.cmd
(Linux)
application/<function_folder> ./deploy.sh
```

## Infrastructure
The Cloud infrastructure is managed by Terraform.

### Initialization
For the first initialization, you will have to create a recipient for Terraform's state. 

Pay attention that GCS bucket's name is global, you need to replace existing bucket names if you see any conflict.

Check the name in infrastructure/terraform/provider.tf, or you will see an error message on launching Terraform commands.

Then initialize terraform's first state. In infrastructure/terraform/, run :
```
terraform init
```

### Create / Modify / Remove
In infrastructure/terraform/, run
```
terraform plan # to see any change
terraform apply
```

## Test
### Unit / Component test
In each function's folder, run
```
npm install
npm test
```
### Integral test
Input cases of :

  - All correct data - e2e/products_eb_test_technique.csv
  - Some incorrect link - e2e/products_eb_test_technique_with_bad_url.csv
  - Some no id - e2e/products_eb_test_technique_with_no_id.csv
  - Some bad image data - 

then check the result.

## CI/CD
This part is managed by bitbucket's pipeline. 

The pipeline's config can be found in the root folder. The gcloud service account's key is written into a variable of Bitbucket's repo.