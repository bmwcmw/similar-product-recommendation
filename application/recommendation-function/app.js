'use strict';

const Cache = require("./cache/cache.repository");
const cron = require("node-cron");

const express = require('express');
const {handleErrorMessage} = require("./error.handler");
const {fetchMatchingInformation} = require("./bigquery/bigquery.helper");

const app = express();

const cache = new Cache();

// Initialize cache on startup
fetchMatchingInformationAndRefreshCache();

function fetchMatchingInformationAndRefreshCache() {
    console.log("Refreshing cache");
    fetchMatchingInformation()
        .then(rows => cache.refreshMatchingDominantColorCache(rows))
        .catch(error => handleErrorMessage(error));
}

const task = cron.schedule("*/30 * * * *", fetchMatchingInformationAndRefreshCache, {
    scheduled: true
});
task.start();

// Entry point of Cloud Function
app.get('/:id', (req, res) => {
    const productId = req.params.id;
    const matchedProducts = cache.getMatchingDominantColor(productId);
    res.json({id: productId, similarId: matchedProducts});
});

module.exports = {
    app
};
