const NodeCache = require('node-cache');

const MATCHING_DOMINANT_COLOR_CACHE_KEY = 'MATCHING_DOMINANT_COLOR';
class CacheRepository {

    constructor() {
        this.cache = new NodeCache({ stdTTL: 0, checkperiod: 0, useClones: false });
    }

    refreshMatchingDominantColorCache(rows) {
        const parsedRows = {};
        for (let i = 0, len = rows.length; i < len; i++) {
            let rowElement = rows[i];
            parsedRows[rowElement.id] = rowElement.matched;
        }

        this.cache.set(MATCHING_DOMINANT_COLOR_CACHE_KEY, parsedRows);
        console.log("Cache refreshed");
    }

    getMatchingDominantColor(id) {
        const dominantColorCache = this.cache.get(MATCHING_DOMINANT_COLOR_CACHE_KEY);
        if (dominantColorCache) {
            return dominantColorCache[id];
        }
        return null;
    }
}

module.exports = CacheRepository;