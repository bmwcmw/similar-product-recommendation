function handleErrorMessage(error) {
    // TODO improvements, tracking, classification, ...
    if (error) {
        console.error(JSON.stringify(error));
    } else {
        console.error('Empty error')
    }
}

function handleErrorMessageAndThrow(error, message = undefined) {
    if (error) {
        handleErrorMessage(message ? message : error);
        throw error;
    }
}

exports.handleErrorMessage = handleErrorMessage;
exports.handleErrorMessageAndThrow = handleErrorMessageAndThrow;