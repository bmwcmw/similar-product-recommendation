'use strict';

const {BigQuery: BigQuery} = require('@google-cloud/bigquery');

const projectId = 'utopian-catfish-2019';
const datasetId = 'product';
const matchingDominantColorTableId = 'MatchingDominantColor';
const bigquery = new BigQuery({
    projectId: projectId,
});

async function fetchMatchingInformation() {
    // Fetch computed matching information
    const fetchBqQuery =
        `WITH MaxDate AS (
          SELECT id, MAX(date_utc) as date_utc FROM \`${datasetId}.${matchingDominantColorTableId}\` GROUP BY id
        )
        SELECT t.id, t.matched FROM \`${datasetId}.${matchingDominantColorTableId}\` t, MaxDate
        WHERE t.id = MaxDate.id
        AND t.date_utc = MaxDate.date_utc`;

    const options = {
        query: fetchBqQuery,
        location: 'EU',
    };

    // Run the query as a job
    const [fetchBqJob] = await bigquery.createQueryJob(options);
    console.log(`Job ${fetchBqJob.id} started.`);

    // Wait for the query to finish
    const [rows] = await fetchBqJob.getQueryResults();
    console.log(`Retrieved ${rows.length} lines.`);

    return Promise.resolve(rows);
}

module.exports = {fetchMatchingInformation};