'use strict';

const Cache = require("../cache/cache.repository");
const test = require(`ava`);

test.serial(`Should correctly refresh cache`, (t) => {
    const cache = new Cache();
    const row1 = {id: 1, matched: 2};
    const row2 = {id: 2, matched: 1};
    const rows = [row1, row2];

    cache.refreshMatchingDominantColorCache(rows);

    t.pass();
});

test.serial(`Should manage cache fetch`, (t) => {
    const cache = new Cache();
    const row1 = {id: 1, matched: 2};
    const row2 = {id: 2, matched: 1};
    const rows = [row1, row2];

    cache.refreshMatchingDominantColorCache(rows);

    t.assert(cache.getMatchingDominantColor(row1.id) === row2.id);
    t.assert(cache.getMatchingDominantColor(row2.id) === row1.id);
    t.assert(!cache.getMatchingDominantColor('nonexistent'));
});