'use strict';

const {BigQuery: BigQuery} = require('@google-cloud/bigquery');

const url = require('url');
const path = require('path');
const {handleErrorMessage} = require("../error.handler");

const projectId = 'utopian-catfish-2019';
const datasetId = 'product';
const productInfoTableId = 'ProductInfo';
const propertyDominantColor = 'PropertyDominantColor';
const visionRefreshStateTableId = 'VisionRefreshState';
const bigquery = new BigQuery({
    projectId: projectId,
});

async function fetchProductInformation(tempFolder) {
    // Fetch new inserted products with photo since last successful refresh
    const fetchBqQuery = `SELECT id, photo, MAX(date_utc) 
    FROM \`${datasetId}.${productInfoTableId}\` 
    WHERE id IS NOT NULL 
    GROUP BY id, photo`;

    const options = {
        query: fetchBqQuery,
        location: 'EU',
    };

    // Run the query as a job
    const [fetchBqJob] = await bigquery.createQueryJob(options);
    console.log(`Job ${fetchBqJob.id} started.`);

    // Wait for the query to finish
    const [imageRows] = await fetchBqJob.getQueryResults();
    console.log(`Retrieved ${imageRows.length} lines.`);

    // Complete photo URL
    for (let i = 0, len = imageRows.length; i < len; i++) {
        imageRows[i].extension = path.extname(url.parse(imageRows[i].photo).pathname);
        imageRows[i].localPath = tempFolder + path.sep + imageRows[i].id + imageRows[i].extension;
        // FIXME: resolve private bucket access error with IAM
        imageRows[i].gcsUrl = `https://storage.googleapis.com/dump-image-folder/${imageRows[i].id + imageRows[i].extension}`;
    }
    return imageRows;
}

function updateDominantColorInBq(dateUtc, dataRow) {
    return new Promise((resolve, reject) => {
        console.log(`Updating dominant color for ${dataRow.id} : ${JSON.stringify(dataRow.dominantColor)}`);
        const dominantColor = dataRow.dominantColor;
        const rows = [{
            id: dataRow.id,
            red: dominantColor.color.red, green: dominantColor.color.green,
            blue: dominantColor.color.blue,
            // alpha: dominantColor.color.alpha,
            // score: dominantColor.score,
            // pixel_fraction: dominantColor.pixelFraction,
            date_utc: dateUtc
        }];

        // Insert data into table
        bigquery
            .dataset(datasetId)
            .table(propertyDominantColor)
            .insert(rows)
            .then(() => resolve())
            .catch(insertErrors => {
                handleErrorMessage(insertErrors);
                reject(new Error('Fetched empty data'));
            });
    }).catch(error => handleErrorMessage(`Error inserting into BigQuery: ${error}`));
}

function updateVisionRefreshState(dateUtc) {
    return new Promise(resolve => {
        console.log(`Updating vision refresh state to ${new Date(dateUtc * 1000)}`);
        const rows = [{
            last_refresh_until_date_utc: dateUtc
        }];
        // Insert data into table
        bigquery
            .dataset(datasetId)
            .table(visionRefreshStateTableId)
            .insert(rows)
            .then(() => resolve())
            .catch(insertErrors => {
                handleErrorMessage(insertErrors);
            });
    }).catch(error => handleErrorMessage(`Error updating refresh state in BigQuery: ${error}`));
}

module.exports = {fetchProductInformation, updateDominantColorInBq, updateVisionRefreshState};