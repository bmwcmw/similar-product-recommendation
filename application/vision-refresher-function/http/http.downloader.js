'use strict';
const https = require('https');
const fs = require('fs');
const path = require('path');
const {handleErrorMessage} = require("../error.handler");
const {handleErrorMessageAndThrow} = require("../error.handler");

// check dev system type
const sysPlatform = process.platform;
const tempFolder = (sysPlatform === "win32" || sysPlatform === "win64") ? 'temp' : '/tmp/vision-function-temp';

function downloadHttpsFile(dataRow) {
    return new Promise((resolve, reject) => {
        let downloadedBytes = 0;
        const request = https.get(dataRow.photo, function (response) {
            if (response.statusCode === 200) {
                // Success, then begin saving to local file
                const fileStream = fs.createWriteStream(dataRow.localPath);
                response.pipe(fileStream);

                response.on('data', function (chunk) {
                    downloadedBytes += chunk.length;
                });

                fileStream.on('finish', function () {
                    if (downloadedBytes === 0) {
                        reject(new Error('Fetched empty data'));
                    }
                });
                resolve(dataRow.localPath);
            } else {
                reject(new Error(`Failed to download ${dataRow.photo} with code ${response.statusCode}`));
            }

            // Add timeout
            request.setTimeout(5000, function () {
                request.abort();
            });
        });
    });
}

function prepareTempFolderSync() {
    // Prepare local temporary folder
    try {
        fs.accessSync(tempFolder, fs.constants.R_OK | fs.constants.W_OK);
        console.log('Temp folder can read/write');
    } catch (err) {
        console.log('No access to temp folder, try to create.');
        // Create the folder
        try {
            fs.mkdirSync(tempFolder, {recursive: true});
        } catch (err) {
            handleErrorMessageAndThrow(err);
        }
    }
    // The folder already exists, delete existing content
    try {
        const files = fs.readdirSync(tempFolder);
        files.forEach(function (file) {
            fs.unlinkSync(path.join(tempFolder, file));
        });
    } catch (err) {
        handleErrorMessageAndThrow(err);
    }
}

function downloadFilesToTemporaryLocalLocation(dataRows, downloadedFileCallback) {
    prepareTempFolderSync();
    const promises = [];
    for (let i = 0, len = dataRows.length; i < len; i++) {
        promises.push(downloadHttpsFile(dataRows[i])
            .then(downloadedFileCallback)
            .catch(error => {
                // might handle retry here
                handleErrorMessageAndThrow(error, `Error while processing file : ${error}`);
            })
        );
    }
    return Promise.all(promises).catch((error) => {
        handleErrorMessage("Finished file downloading with error");
        handleErrorMessage(error);
    });
}

module.exports = {downloadFilesToTemporaryLocalLocation, tempFolder};
