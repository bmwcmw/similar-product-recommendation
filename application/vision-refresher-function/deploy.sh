#!/usr/bin/env bash
gcloud functions deploy refreshSinceLastTime --runtime nodejs10 --trigger-topic vision-refresh-command --region europe-west1 --project utopian-catfish-2019