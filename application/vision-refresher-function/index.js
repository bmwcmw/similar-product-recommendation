'use strict';

const bigQueryHelper = require('./bigquery/bigquery.helper');
const httpDownloader = require('./http/http.downloader');
const gcsUploader = require('./gcs/gcs.uploader');
const {dumpImageBucket} = require("./gcs/gcs.uploader");
const {fetchVisionData} = require("./vision/vision.api.caller");

// We have to clarify the rate control.
// Currently it gets all rows from BQ and run the vision API in a promise.all() way.

// Entry point of Cloud Function
exports.refreshSinceLastTime = async (data, context) => {
    // Check data message by another specification
    console.log(`Received command ${data} - ${context}`);

    // Beginning time of this action
    const dateUtc = new Date().getTime() / 1000;

    // Retrieve information lines from Bq for images haven't processed since last execution of this function
    const fetchedRowsFromBq = await bigQueryHelper.fetchProductInformation(httpDownloader.tempFolder);

    if (fetchedRowsFromBq.length < 1) {
        console.log(`No new product image to update, aborting.`);
        return;
    }

    // Download files to local storage
    // Then Upload files to GCS to avoid issue of Vision API(bot rejected ?)
    await gcsUploader.removeFilesInBucket(dumpImageBucket);
    await httpDownloader.downloadFilesToTemporaryLocalLocation(fetchedRowsFromBq,
        fileRow => gcsUploader.uploadFileToBucket(fileRow)
    ).then(() => console.log(`${fetchedRowsFromBq.length} files processed.`));

    // Fetch result of images in GCS from Vision API, and update concerned tables
    await fetchVisionData(fetchedRowsFromBq,
        visionData => {
            bigQueryHelper.updateDominantColorInBq(dateUtc, visionData);
        })
        .then(async () => {
            console.log(`Vision data processed for ${fetchedRowsFromBq.length} files.`);
            await bigQueryHelper.updateVisionRefreshState(dateUtc);
        });
};
