'use strict';

const test = require(`ava`);
const {downloadFilesToTemporaryLocalLocation} = require("../http/http.downloader");
const {tempFolder} = require("../http/http.downloader");
const path = require(`path`);

test.serial(`Should continue downloading test data files even with error`, async (t) => {
    await downloadFilesToTemporaryLocalLocation([
            {
                id: 'bad-image-data',
                localPath: tempFolder + path.sep + 'bad-image-data.jpg',
                photo: 'https://storage.googleapis.com/dump-image-folder-test/bad-image-data.jpg'
            },
            {
                id: 'good-image-data',
                localPath: tempFolder + path.sep + 'good-image-data.jpg',
                photo: 'https://storage.googleapis.com/dump-image-folder-test/good-image-data.jpg'
            },
            {
                id: 'nonexistent-image-data',
                localPath: tempFolder + path.sep + 'nonexistent-image-data.jpg',
                photo: 'https://storage.googleapis.com/dump-image-folder-test/nonexistent-image-data.jpg'
            }],
        visionResult => {
            console.log(JSON.stringify(visionResult));
        });
    t.pass();
});