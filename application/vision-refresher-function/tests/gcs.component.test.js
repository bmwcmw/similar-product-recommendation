'use strict';

const test = require(`ava`);

test.serial(`Fails with nonexistent bucket`, async t => {
    const {removeFilesInBucket} = require("../gcs/gcs.uploader");
    await t.throwsAsync(async () => {
        await removeFilesInBucket('nonexistent-bucket');
    });
});

test.serial(`Should replay standard process without error`, async (t) => {
    const path = require(`path`);
    const fs = require('fs');
    const {Storage} = require('@google-cloud/storage');
    const storage = new Storage();
    const {dumpImageBucket} = require("../gcs/gcs.uploader");
    const {uploadFileToBucket} = require("../gcs/gcs.uploader");
    const {tempFolder} = require("../http/http.downloader");
    const {handleErrorMessage} = require("../error.handler");

    const testBucketName = `${dumpImageBucket}-test-${new Date().getTime()}`;
    const testBucket = storage.bucket(testBucketName);
    await testBucket.create()
        .then(() => {
            const localTestFilePath = path.join(tempFolder, 'test.txt');
            fs.mkdirSync(tempFolder, {recursive: true});
            console.log(`Test folder created`);
            fs.writeFileSync(localTestFilePath, 'TEST DATA');
            uploadFileToBucket(localTestFilePath).then(() => {
                console.log(`Test file successfully uploaded`);
                fs.unlinkSync(localTestFilePath);
            });
        })
        .catch(error => {
            handleErrorMessage(error);
        })
        .finally(() => {
            testBucket.delete();
            const files = fs.readdirSync(tempFolder);
            files.forEach(function (file) {
                fs.unlinkSync(path.join(tempFolder, file));
            });
        });

    t.pass();
});