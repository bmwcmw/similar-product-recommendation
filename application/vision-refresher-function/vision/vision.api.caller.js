'use strict';
const vision = require('@google-cloud/vision');
const {handleErrorMessageAndThrow} = require("../error.handler");
const {handleErrorMessage} = require("../error.handler");

const visionClient = new vision.ImageAnnotatorClient();

function fetchDominantColorFromVisionAPI(dataRow, fetchedVisionDataCallback) {
    const id = dataRow.id;
    const url = dataRow.gcsUrl;
    return visionClient.imageProperties(url).then(async result => {
        const visionResult = result === undefined || result.length < 1 ? null : result[0];
        if (!visionResult) {
            let errorMessage = `Fetched null response for ${id}`;
            handleErrorMessageAndThrow(errorMessage);
        }
        if (visionResult.error) {
            handleErrorMessageAndThrow(visionResult.error)
        }
        if (!visionResult.imagePropertiesAnnotation) {
            const errorMessage = `No imagePropertiesAnnotation for ${id}`;
            handleErrorMessageAndThrow(errorMessage);
        }
        console.log(`Fetched response from Vision API for ${id}`);
        const colors = visionResult.imagePropertiesAnnotation.dominantColors.colors;
        let dominantColor;
        let maxScore = 0;
        colors.forEach(color => {
            if (color.score > maxScore) {
                maxScore = color.score;
                dominantColor = color;
            }
        });
        await fetchedVisionDataCallback({id, url, dominantColor});
    });
}

function fetchVisionData(dataRows, fetchedVisionDataCallback) {
    const promises = [];
    for (let i = 0, len = dataRows.length; i < len; i++) {
        promises.push(fetchDominantColorFromVisionAPI(dataRows[i], fetchedVisionDataCallback));
    }
    return Promise.all(promises).catch((error) => {
        handleErrorMessage("Finished Vision API fetching with error");
        handleErrorMessage(error);
    });
}

module.exports.fetchVisionData = fetchVisionData;
