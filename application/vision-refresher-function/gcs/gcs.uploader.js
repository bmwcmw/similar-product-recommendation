'use strict';

const {handleErrorMessage} = require("../error.handler");
const {Storage} = require('@google-cloud/storage');

const storage = new Storage();
const dumpImageBucket = "dump-image-folder";

function removeFilesInBucket(imageBucket) {
    return new Promise((resolve, reject) => {
        storage.bucket(imageBucket).deleteFiles({
            force: true
        }, function (err, file) {
            if (err) {
                handleErrorMessage(err);
                reject(err);
            } else {
                console.log(`Bucket ${imageBucket} cleaned for file ${file}.`);
                resolve();
            }
        });
    });
}

function uploadFileToBucket(localPath) {
    return new Promise(resolve => {
        storage.bucket(dumpImageBucket).upload(localPath, {
            metadata: {
                cacheControl: 'no-cache',
            },
            resumable: true
        }, function (err, file) {
            if (err) {
                handleErrorMessage(err, `Error uploading ${file} to ${dumpImageBucket}`);
            }
            console.log(`${localPath} uploaded to ${dumpImageBucket}.`);
            resolve();
        });
    });
}

module.exports = {uploadFileToBucket, removeFilesInBucket, dumpImageBucket};
