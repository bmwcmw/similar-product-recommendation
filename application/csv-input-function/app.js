'use strict';

const {Storage} = require('@google-cloud/storage');
const {BigQuery} = require('@google-cloud/bigquery');
const csv = require('csv-parser');

const projectId = 'utopian-catfish-2019';
const storage = new Storage({
    projectId: projectId,
});

const datasetId = 'product';
const tableId = 'ProductInfo';
const bigquery = new BigQuery({
    projectId: projectId,
});

function handleError(error) {
    console.error(JSON.stringify(error))
}

function parseDataFromStream(stream) {
    const dataArray = [];

    // Beginning time of this action
    const dateUtc = new Date().getTime() / 1000;

    return new Promise(resolve => {
        stream
            .pipe(csv({separator: ';'}))
            .on('data', function (data) {
                try {
                    data.date_utc = dateUtc;
                    dataArray.push(data);
                } catch (err) {
                    handleError(err);
                }
            })
            .on('finish', function () {
                console.log(`Successfully parsed file with ${dataArray.length} lines`);
                resolve(dataArray);
            });
    });
}

function insertRowsAsStream(dataArray) {
    return new Promise(resolve => {
        bigquery
            .dataset(datasetId)
            .table(tableId)
            .insert(dataArray)
            .then(() => {
                console.log(`Successfully inserted ${dataArray.length} lines`);
                resolve();
            })
            .catch(error => handleError(error));
    });
}

// Entry point of Cloud Function
exports.inputImageCSVtoBQ = async (data, context) => {
    // Receive created or modified file event
    const eventFile = data;
    console.log(`  Event ${context.eventId}`);
    console.log(`  Event Type: ${context.eventType}`);
    console.log(`  Bucket: ${eventFile.bucket}`);
    console.log(`  File: ${eventFile.name}`);
    console.log(`  Metageneration: ${eventFile.metageneration}`);
    console.log(`  Created: ${eventFile.timeCreated}`);
    console.log(`  Updated: ${eventFile.updated}`);

    const bucket = storage.bucket(eventFile.bucket);
    const gcsInputFile = bucket.file(eventFile.name);

    const fileStream = gcsInputFile.createReadStream();

    await parseDataFromStream(fileStream).then(insertRowsAsStream).then(() => console.log("Finished."));
};

exports.parseDataFromStream = parseDataFromStream;
