'use strict';

import {parseDataFromStream} from "../app";

const fs = require(`fs`);
const path = require(`path`);
const proxyquire = require(`proxyquire`).noCallThru();
const sinon = require(`sinon`);
const test = require(`ava`);

const filename = `../../products_eb_test_technique.csv`;

function initMock() {
    const filePath = path.join(__dirname, `${filename}`);
    const file = {
        createReadStream: () => fs.createReadStream(filePath, {encoding: `utf8`})
    };
    const bucket = {
        file: sinon.stub().returns(file)
    };
    const storage = {
        bucket: sinon.stub().returns(bucket)
    };
    const StorageMock = sinon.stub().returns(storage);

    return {
        program: proxyquire(`../`, {
            '@google-cloud/storage': StorageMock
        }),
        mocks: {
            Storage: StorageMock,
            storage: storage,
            bucket: bucket,
            file: file
        }
    };
}

test.serial(`Fails without bucket`, (t) => {
    const expectedMsg = `Make sure you have "bucket" property in your request`;

    t.throws(
        () => initMock().program.loadFile({data: {name: `file`}}),
        Error,
        expectedMsg
    );
});

test.serial(`Fails without file`, (t) => {
    const expectedMsg = `Make sure you have "file" property in your request`;

    t.throws(
        () => initMock().program.loadFile({data: {bucket: `bucket`}}),
        Error,
        expectedMsg
    );
});

test.serial(`Test parse data from local file in 500 ms`, async (t) => {
    const fs = require('fs'), fileStream = fs.createReadStream('./tests/products_eb_test_technique_short.csv');
    const successParsedDataCallback = (dataArray) => {
        t.assert(dataArray.length > 0);
    };
    parseDataFromStream(fileStream).then(successParsedDataCallback);

    const sleep = (ms) => {
        return new Promise(resolve => {
            setTimeout(resolve, ms)
        })
    };
    await sleep(500);
});