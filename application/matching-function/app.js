const {rgb2lab} = require("colour-proximity");
const {BigQuery} = require('@google-cloud/bigquery');

const projectId = 'utopian-catfish-2019';
const dataSetId = 'product';
const dominantColorTableId = 'PropertyDominantColor';
const matchingDominantColorTableId = "MatchingDominantColor";
const bigquery = new BigQuery({
    projectId: projectId,
});

const NB_NEIGHBORS = 6;

exports.matchingProduct = async (req, res) => {
    await processDominantColor(NB_NEIGHBORS);
    res.send(`Matching process finished.`);
};

const proximity = (rgb1, rgb2) => {
    const c1 = rgb2lab(rgb1);
    const c2 = rgb2lab(rgb2);
    return Math.sqrt(Math.pow(c1[0] - c2[0], 2) + Math.pow(c1[1] - c2[1], 2) + Math.pow(c1[2] - c2[2], 2));
};

exports.proximity = proximity;

const ascendingNumberFieldComparator = (field) => {
    return function (a, b) {
        return a[field] - b[field];
    }
};

exports.ascendingNumberFieldComparator = ascendingNumberFieldComparator;

// compute n first neighbors of each row
function match(rows, n) {
    const matchedResult = {};
    rows.forEach(row => {
        const id = row.id;
        console.log(`Matching ${id}`);
        const neighbors = [];
        const toMatchRGB = [row.red, row.green, row.blue];
        rows.forEach(anotherRow => {
            const anotherId = anotherRow.id;
            if (id === anotherId) {
                // Do not match with itself
                return;
            }
            const anotherRGB = [anotherRow.red, anotherRow.green, anotherRow.blue];
            const currentProximity = proximity(toMatchRGB, anotherRGB);

            neighbors.push({
                id: anotherId,
                proximity: currentProximity
            });
        });
        matchedResult[id] = neighbors.sort(ascendingNumberFieldComparator('proximity')).slice(0, n);
    });
    return matchedResult;
}

exports.match = match;

async function processDominantColor(n) {
    // Beginning time of this action
    const dateUtc = new Date().getTime() / 1000;

    const latestDominantColorQuery = `SELECT id, red, green, blue, MAX(date_utc) AS date_utc
    FROM \`${dataSetId}.${dominantColorTableId}\` 
    WHERE id IS NOT NULL GROUP BY id, red, green, blue`;

    const options = {
        query: latestDominantColorQuery,
        // Location must match that of the dataset(s) referenced in the query.
        location: 'EU',
    };

    // Run the query as a job
    const [job] = await bigquery.createQueryJob(options);
    console.log(`Job ${job.id} started.`);

    // Wait for the query to finish
    const [rows] = await job.getQueryResults();
    const matchedResult = match(rows, n);
    console.log(`Computed matching for ${Object.keys(matchedResult).length} rows`);

    const arrayToInsertBq = [];
    for (const [key, value] of Object.entries(matchedResult)) {
      const row = {};
      row.id = key;
      row.matched = value;
      row.date_utc = dateUtc;
      arrayToInsertBq.push(row);
    }
    console.log(`Prepared ${Object.keys(arrayToInsertBq).length} rows to insert into BQ`);
    insertRowsAsStream(arrayToInsertBq, dataSetId, matchingDominantColorTableId);
}

function insertRowsAsStream(dataArray, datasetId, tableId) {
    return bigquery
        .dataset(datasetId)
        .table(tableId)
        .insert(dataArray)
        .catch(err => console.log(err.stack));
}
