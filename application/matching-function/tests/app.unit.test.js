'use strict';

import {proximity} from "../app";

const {match} = require("../app");
const test = require(`ava`);
const {ascendingNumberFieldComparator} = require("../app");

test.serial(`Should compute relatively correct proximity`, (t) => {
    const color1 = [0, 0, 0];
    const color2 = [1, 1, 1];
    const color3 = [2, 2, 2];
    const proximity12 = proximity(color1, color2);
    const proximity13 = proximity(color1, color3);
    t.assert(proximity13 > proximity12);
});

test.serial(`Should match most similar product`, async (t) => {
    const row1 = {id: 1, red: 0, green: 0, blue: 0};
    const row2 = {id: 2, red: 1, green: 1, blue: 1};
    const row3 = {id: 3, red: 3, green: 3, blue: 3};
    const row4 = {id: 4, red: 0.5, green: 0.5, blue: 0.2};
    const rows = [row1, row2, row3, row4];

    const matchedResult = match(rows, 1);

    console.log(JSON.stringify(matchedResult));
    t.assert(matchedResult[row1.id][0].id === row4.id);
    t.assert(matchedResult[row2.id][0].id === row4.id);
    t.assert(matchedResult[row3.id][0].id === row2.id);
    t.assert(matchedResult[row4.id][0].id === row1.id);
});

test.serial(`Should sort correctly`, (t) => {
    const arrayReverse = [{id:3, score:3}, {id:2, score:2}, {id:1, score:1}];
    const array = [{id:1, score:1}, {id:2, score:2}, {id:3, score:3}];
    const sortedArrayReverse = arrayReverse.sort(ascendingNumberFieldComparator('score'));
    const sortedArray = array.sort(ascendingNumberFieldComparator('score'));
    t.assert(JSON.stringify(sortedArray) === JSON.stringify(sortedArrayReverse) );
});
