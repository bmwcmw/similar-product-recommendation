resource "google_cloud_scheduler_job" "vision_refresh_scheduler" {
  description = "Refresh vision properties"
  name        = "vision-refresh-scheduler"
  region      = "europe-west1"
  schedule    = "*/10 * * * *"

  pubsub_target {
    topic_name = "${google_pubsub_topic.vision_refresh_command_topic.id}"
    data       = "${base64encode("refresh")}"
  }
}

