# used to store our terraform.tfstate
terraform {
  backend "gcs" {
    bucket = "terraform-utopian-catfish-2019"
    prefix = "product-catalog"
  }
}

provider "google" {
  region  = "${var.region}"
  project = "${var.project_name}"
}

