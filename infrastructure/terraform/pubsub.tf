resource "google_pubsub_topic" "vision_refresh_command_topic" {
  name = "vision-refresh-command"
}

resource "google_pubsub_topic_iam_binding" "pubsub_publisher" {
  topic = "vision-refresh-command"
  role  = "roles/pubsub.publisher"
  members = [
    "serviceAccount:${google_service_account.product-catalog.email}",
  ]
}

resource "google_pubsub_topic_iam_binding" "pubsub_subscriber" {
  topic = "vision-refresh-command"
  role  = "roles/pubsub.subscriber"
  members = [
    "serviceAccount:${google_service_account.product-catalog.email}",
  ]
}