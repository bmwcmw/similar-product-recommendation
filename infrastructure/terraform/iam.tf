resource "google_service_account" "product-catalog" {
  account_id   = "product-catalog"
  display_name = "Product Catalog Service Account"
}

resource "google_storage_bucket_iam_member" "csv-input-bucket-admin" {
  bucket = "${google_storage_bucket.csv-input.name}"
  role   = "roles/storage.objectAdmin"
  member = "serviceAccount:${google_service_account.product-catalog.email}"
}