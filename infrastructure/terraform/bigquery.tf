resource "google_bigquery_dataset" "product_dataset" {
  dataset_id = "product"
  location   = "EU"
  project    = "${var.project_name}"

  labels = {
    feature = "${var.feature}"
  }

  access {
    role          = "READER"
    special_group = "projectReaders"
  }

  access {
    role          = "WRITER"
    special_group = "projectWriters"
  }

  access {
    role          = "OWNER"
    special_group = "projectOwners"
  }

  access {
    role          = "WRITER"
    user_by_email = "${google_service_account.product-catalog.email}"
  }
}

resource "google_bigquery_table" "product_info" {
  dataset_id = "${google_bigquery_dataset.product_dataset.dataset_id}"
  table_id   = "ProductInfo"
  schema     = file("./files/product-info.json")

  labels = {
    feature = "${var.feature}"
  }
}

resource "google_bigquery_table" "vision_refresh_state" {
  dataset_id = "${google_bigquery_dataset.product_dataset.dataset_id}"
  table_id   = "VisionRefreshState"
  schema     = file("./files/vision-refresh-state.json")
}

resource "google_bigquery_table" "property_dominant_color" {
  dataset_id = "${google_bigquery_dataset.product_dataset.dataset_id}"
  table_id   = "PropertyDominantColor"
  schema     = file("./files/property-dominant-color.json")
  labels = {
    feature = "${var.feature}"
  }
}

resource "google_bigquery_table" "matching_dominant_color" {
  dataset_id = "${google_bigquery_dataset.product_dataset.dataset_id}"
  table_id   = "MatchingDominantColor"
  schema     = file("./files/matching-dominant-color.json")
  labels = {
    feature = "${var.feature}"
  }
}