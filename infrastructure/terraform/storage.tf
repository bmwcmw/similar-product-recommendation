resource "google_storage_bucket" "csv-input" {
  name          = "file-input-folder"
  location      = "EU"
  storage_class = "MULTI_REGIONAL"

  labels = {
    feature = "${var.feature}"
  }
}

resource "google_storage_bucket" "images" {
  name          = "dump-image-folder"
  location      = "EU"
  storage_class = "MULTI_REGIONAL"
}

resource "google_storage_default_object_acl" "image-bucket-default-acl" {
  bucket = "${google_storage_bucket.images.name}"
  role_entity = [
    "READER:allUsers",
  ]
}

resource "google_storage_bucket" "images-test" {
  name          = "dump-image-folder-test"
  location      = "EU"
  storage_class = "MULTI_REGIONAL"
}

resource "google_storage_default_object_acl" "image-test-bucket-default-acl" {
  bucket = "${google_storage_bucket.images-test.name}"
  role_entity = [
    "READER:allUsers",
  ]
}