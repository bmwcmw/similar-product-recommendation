variable "feature" {
  default = "product-catalog"
}

variable "region" {
  default = "europe-west1"
}

variable "zone" {
  default = "europe-west1-d"
}

variable "project_name" {
  description = "The ID of the Google Cloud project"
  default     = "utopian-catfish-2019"
}

